const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    adress: { type: String, required: true },
    orders: [{ type: mongoose.Types.ObjectId, ref: 'Order' }],
    role: {
        type: String, enum: ['user', 'admin'],
        default: 'user', required: true
    }
},
    { timestamps: true }
)
const User = mongoose.model('User', usersSchema)
module.exports = User;