const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cartsSchema = new Schema({
    owner: { type: mongoose.Types.ObjectId, ref: 'User', required: true },
    products: [{
        productId: { type: mongoose.Types.ObjectId, ref: 'Product' },
        quantity: { type: Number, required: true }
    }],
    totalPrice: { type: Number, required: true, default: 0 },
    totalQuantity: { type: Number, required: true, default: 0 },
},
    {
        timestamps: true
    }
);

const Cart = mongoose.model('Carts', cartsSchema);


module.exports = Cart