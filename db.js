const mongoose = require('mongoose');
require('dotenv').config()
const DB_URL = process.env.DB_URL || 'mongodb://localhost:27017/Ecommerce';


const connect = async () => {
    try {
        const db = await mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('Conected to data base', db.connection.host);
    } catch (error) {
        console.log('There has been an error conecting to the data base');
    }
};

module.exports = {
    DB_URL,
    connect
};