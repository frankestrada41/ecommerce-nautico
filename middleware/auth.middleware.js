const alert = require('alert')

const isAuth = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    else {
        return res.render('login')
    }
}

const isAdmin = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role === 'admin') {
            return next();
        } else {
            alert('You need to be an admin to access')
            return res.redirect('/')
        }
    }
    else {
        return res.redirect('/auth/login')
    }
}

module.exports = {
    isAuth,
    isAdmin,
}