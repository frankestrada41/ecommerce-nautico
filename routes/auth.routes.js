const express = require('express');
const passport = require('passport');
const router = express.Router();
const Product = require('../models/Products.model')

router.get('/register', (req, res, next) => {

    return res.render('register')

});

router.post('/register', (req, res, next) => {

    const { email, password, name } = req.body;
    if (!email || !password || !name) {
        const error = new Error('Completa todos los campos')
        return res.render('error', { error });
    }
    const done = (error, user) => {
        if (error) {
            console.log('erro registrating --> ', error)
            return res.render('error', { message: error.message })
        }

        req.logIn(user, (error, user) => {

            if (error) return next(error);
            return res.redirect('/')
        })
    }
    passport.authenticate('register', done)(req);
});

router.get('/login', (req, res, next) => {
    try {
        res.render('login')

    } catch (error) {
        return next(error)
    }
})

router.post('/login', (req, res, next) => {
    const { email, password } = req.body
    if (!email || !password) {
        const error = new Error('Completa todos los campos')
        return res.render('login', { error })
    }

    const done = (error, user) => {

        if (error) {
            return next(error);
        }
        // req.logIn(user, (error, user) => {

        //     if (error) return next(error)

        //     return res.redirect('/')
        // })
        const doneParaSerialize = (error, user) => {
            if (error) {
                return next(error)
            }
            return res.redirect('/')

        }
        req.logIn(user, doneParaSerialize)
    }
    passport.authenticate('login', done)(req);
})

router.post('/logout', (req, res, next) => {
    if (req.user) {
        req.logOut();

        req.session.destroy(() => {
            res.clearCookie('connect.sid')
            return res.redirect('/')
        })
    }
    else {
        return res.status(200).json('No habia nadie logeado')
    }
})



module.exports = router;