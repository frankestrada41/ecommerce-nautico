const express = require('express')
const mongoose = require('mongoose')
const { isAuth } = require('../middleware/auth.middleware')
const Cart = require('../models/Cart.model')
const Product = require('../models/Products.model')


const router = express.Router()


router.get('/', [isAuth], async (req, res, next) => {
    try {
        const existingCart = await Cart.findOne({ owner: req.user._id }).populate('owner').populate('products.productId')
        if (existingCart) {
            existingCart.owner.password = null
            // return res.json(existingCart)
            return res.render('cart', { existingCart })
        }
        else {
            return res.render('nocart')
        }


    } catch (error) {
        return next(error)
    }
})

router.post('/add-to-cart/:id', async (req, res, next) => {
    const userId = req.user._id;
    const productId = req.params.id
    const product = await Product.findById(productId)

    const existingCart = await Cart.findOne({ owner: userId }).populate('owner');
    try {
        if (!existingCart) {
            const newCart = new Cart({
                owner: userId,
                products: [{
                    productId,
                    quantity: 1,
                }],
                totalPrice: product.price,
                totalQuantity: 1,
            })
            await newCart.save();
        } else {
            const hasProduct = existingCart.products.some((prod => prod.productId.equals(productId)))
            if (!hasProduct) {
                const editedCart = await Cart.findByIdAndUpdate(
                    existingCart._id,
                    {
                        $addToSet: {
                            products: {
                                productId: mongoose.Types.ObjectId(productId),
                                quantity: 1,
                            }
                        },
                        $inc: {
                            totalPrice: product.price,
                            totalQuantity: 1
                        }

                    },
                    { new: true }

                )
                return res.redirect('/cart')
            } else {
                const newProducts = existingCart.products.map(product => {
                    if (product.productId.equals(productId)) {
                        product.quantity = product.quantity + 1;
                    }
                    return product;
                })
                const editedProduct = await Cart.findByIdAndUpdate(
                    existingCart._id,
                    {
                        $set: {
                            products: newProducts,
                        },
                        $inc: {
                            totalPrice: product.price,
                            totalQuantity: 1
                        }
                    },
                    { new: true }
                );

            }
        }
        return res.redirect('/cart')
    } catch (error) {
        return next(error)
    }

})

router.delete('/delete', [isAuth], async (req, res, next) => {

})

module.exports = router