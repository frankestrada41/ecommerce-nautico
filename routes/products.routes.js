const express = require('express');
const router = express.Router();
const Product = require('../models/Products.model')
const { upload, uploadToCloudinary } = require('../middleware/file.middleware')
const { isAuth, isAdmin } = require('../middleware/auth.middleware')



router.get('/', [isAuth], async (req, res, next) => {
    try {
        const allproducts = await Product.find()
        return res.render('products', { allproducts })

    } catch (error) {
        return next(error)

    }
});

router.get('/create', [isAdmin], (req, res, next) => {
    try {
        return res.render('create-product')
    } catch (error) {
        return next(error)
    }
});

router.post('/create', [isAuth, upload.single('image'), uploadToCloudinary], async (req, res, next) => {
    try {
        const { name, category, description, price, rating, coments } = req.body
        const image = req.fileUrl ? req.fileUrl : '';
        if (!name || !category || !description || !price) {
            const error = new Error('all fields must be complete');
            return res.render('error', { error })
        }
        const newProduct = new Product({ name, category, description, price, rating, coments, image });
        const createProduct = await newProduct.save();
        return res.redirect('/products')

    } catch (error) {
        return next(error)
    }
})
router.get('/edit/:id', async (req, res, next) => {
    const { id } = req.params;
    try {
        const product = await Product.findById(id)
        return res.render('edit-product', { product })
    } catch (error) {
        return next(error);
    }
})
router.post('/edit/:id', [isAdmin, upload.single('image'), uploadToCloudinary], async (req, res, next) => {
    const { id } = req.params;
    const { name, category, description, price } = req.body;
    const uploadFields = { name, category, description, price };

    if (req.fileUrl) {
        uploadFields.image = req.fileUrl;
    }

    try {
        console.log('postdeEdit')
        console.log(req.body)
        const editProduct = await Product.findByIdAndUpdate(
            id,
            uploadFields,
            { new: true }
        );
        return res.redirect('/products')
    } catch (error) {
        return next(error);
    }
})

router.delete('/delete/:id', [isAdmin], async (req, res, next) => {

    try {
        const { id } = req.params;
        const productToDelete = await Product.findByIdAndDelete(id)
        if (productToDelete) {
            const products = await Product.find();
            return res.redirect('/products')
        } else {
            const error = new Error("Can't find a product with this id. ¿Are you sure?");
            error.status = 400;
            return res.render('error', { message: error.message, status: error.status });
        }

    } catch (error) {
        return next(error)
    }
})

module.exports = router