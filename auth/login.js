const LocalStrategy = require('passport-local').Strategy
const User = require('../models/User.model');
const passport = require('passport')
const bcrypt = require('bcrypt');


passport.serializeUser((user, done) => {
    return done(null, user._id)
})

passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser)
    } catch (error) {
        return done(error, null)
    }
})

const loginStrategy = new LocalStrategy(

    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {

        try {
            let existingUser = await User.findOne({ email })

            if (!existingUser) {
                const error = new Error('El usuario no existe')
                error.status = 401
                return done(error, null)
            }

            const isVaildPassword = await bcrypt.compare(password, existingUser.password);

            if (!isVaildPassword) {
                const error = new Error('la contraseña es incorrecta');
                return done(error, null);
            }

            existingUser.password = null;
            console.log('estrategia login', existingUser)
            return done(null, existingUser)

        } catch (error) {
            console.log('te ha petado por esto---->', error)
            return done(error)
        }

    });

passport.use('login', loginStrategy);