const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User.model');
const passport = require('passport')
const { validateEmail, validatePassword } = require('../auth/util');
const bcrypt = require('bcrypt')

passport.serializeUser((user, done) => {
    return done(null, user._id)
})

passport.deserializeUser(async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser)
    } catch (error) {
        return done(error, null)
    }
})

const registerStrategy = new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {

        try {
            const existingUser = await User.findOne({ email });
            if (existingUser) {

                const error = new Error('El usuario ya está registrado')
                return done(error, null)
            }
            if (!validateEmail(email)) {

                const error = new Error('El campo email tiene que contener un email')
                return done(error, null)

            }
            if (!validatePassword(password)) {

                const error = new Error('La contraseña no tiene las caracteristicas necesarias')
                return done(error, null)
            }

            const saltRounds = 10;
            const passwordHash = await bcrypt.hash(password, saltRounds);

            const newUser = new User(

                {
                    email: email,
                    password: passwordHash,
                    name: req.body.name,
                    adress: req.body.adress

                }

            )
            console.log('nuevo usuario', newUser)
            const savedUser = await newUser.save()

            savedUser.password = null
            return done(null, savedUser)

        } catch (error) {
            return done(error, null)
        }
    });

passport.use('register', registerStrategy);
