const passport = require('passport');
const registerStrategy = require('./register');

passport.use('register', registerStrategy);