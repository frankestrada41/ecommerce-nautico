const Product = require('../models/Products.model');
const mongoose = require('mongoose');
const db = require('../db');

const productsSeed = [
    {
        'name': 'Jet Ski',
        'category': 'Motor',
        'description': 'The perfect Jet Ski for your sea expirience!',
        'price': 12000,
        'rating': 5,
        'coments': [],
        'image': 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRBnpYzTekPSHn9KWvhDyza1Zn_iDqjZ0hkA&usqp=CAU',
    }
];

mongoose.connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {
        const allProducts = await Product.find();
        if (allProducts.length) {
            await Product.collection.drop();
        }
    })
    .then(async () => {
        await Product.insertMany(productsSeed);
    })
    .catch((error) => {
        console.log('We couldn´t insert the seed to the data base')
    })
    .finally(() => {
        mongoose.disconnect();
    })