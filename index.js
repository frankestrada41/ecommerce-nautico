const express = require('express');
const path = require('path');
const db = require("./db");
const dotenv = require('dotenv')
const methodoverride = require('method-override')
const indexRoute = require('./routes/index.routes');
const authRoute = require('./routes/auth.routes');
const productsRoute = require('./routes/products.routes')
const cartRouth = require('./routes/cart.routes')
const passport = require('passport');
require('./auth/register');
require('./auth/login');
const MongoStore = require('connect-mongo')

dotenv.config();
db.connect();

const PORT = process.env.PORT || 3400;


const server = express();
const session = require('express-session');

server.use(methodoverride('_method'))
server.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL })

}))

server.use(express.json());
server.use(express.urlencoded({ extended: true }));
server.use(passport.initialize());
server.use(passport.session());
server.use(express.static(path.join(__dirname, 'public')));



server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

server.use('/', indexRoute);
server.use('/auth', authRoute);
server.use('/products', productsRoute);
server.use('/cart', cartRouth)

server.use('*', (req, res, next) => {
    const error = new Error('Page not found');
    console.log(error)
    return res.status(404).render('error', { message: error.message, status: error.status });
});

server.use((error, req, res, next) => {
    console.log(error);
    return res.status(error.status || 500).render('error',
        {
            message: error.message || 'Unexpected error',
            status: error.status || 500,
        });
});

server.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT} `);
});